# Trabajo - Taller de tecnologias moviles

**Nombre:** Juan Carlos Fernando Saavedra Peña
**Carrera:** Ingenieria Informatica

## Estado entrega
 
El trabajo consiste en lo siguiente:

1.- Se le pide que cree página con una lista (como en el primer programa, 
lista de héroes y sus colores), pero ahora debe obtener los datos desde un
servicio a través de internet y NO desde los assets (utilizar el archivo 
heroes.json colgado en nuestra página: hectoralvarez.dev/icc727/heroes.json).

De los puntos anteriores se pudieron cumplir los siguientes puntos:

1. Se creo vista que genera lista de superheroes.
2. Dicha lista se genera en base al archivo "heroes.json" que se obtiene desde el sitio web indicado.
3. Se generan iconos con el respectivo color de cada heroe.

