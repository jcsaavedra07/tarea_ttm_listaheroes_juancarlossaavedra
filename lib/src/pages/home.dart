import 'package:flutter/material.dart';

import 'package:tarea_listas_ttm_jcsp/src/utils/connected.dart';
import 'package:tarea_listas_ttm_jcsp/src/services/test_service.dart';
import 'package:tarea_listas_ttm_jcsp/src/models/mis_heroes_model.dart';
import 'package:tarea_listas_ttm_jcsp/src/utils/json_icon_util.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String mnsjConn = "";
  MisHeroesModel heroes = MisHeroesModel();

  Future<bool> checkConn() async {
    bool connAvailable = await Connected().isConnected();

    (connAvailable)
        ? this.mnsjConn = "Si hay conexion"
        : this.mnsjConn = "No hay conexion";

    return connAvailable;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lista de heroes"),
      ),
      body: _getHeroes(),
    );
  }

  Widget _getHeroes() {
    return FutureBuilder(
      future: TestService().getLista(),
      initialData: [],
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        List<Widget> listaHeroes = new List<Widget>();
        snapshot.data.forEach((heroe) {
          Column temp = Column(
            children: <Widget>[
              ListTile(
                title: Text(heroe.nombre.toString()),
                leading: getIcon(heroe.icon.toString(), heroe.color),
                onTap: () => print(
                    "Hola soy ${heroe.nombre}, mi poder es ${heroe.poder}"),
              ),
              Divider()
            ],
          );

          listaHeroes.add(temp);
        });

        return ListView(
          children: listaHeroes,
        );
      },
    );
  }
}
